---
title: Forgejo monthly update - March 2023
publishDate: 2023-04-10
tags: ['news']
excerpt: Forgejo v1.19, the first major upgrade, was published by the release team. Although still experimental, Forgejo Actions is now used daily to develop and maintain the runner. The `setup-forgejo` action was created to spawn a Forgejo instance for the duration of a test.
---

[Forgejo v1.19](https://codeberg.org/forgejo/forgejo/releases/tag/v1.19.0-2) was published. It is the first major upgrade and was a critical milestone to verify the development strategy of the soft fork actually works. The release team was [legitimized by the community](https://codeberg.org/forgejo/governance/issues?q=application%20to%20release%20team&type=all&sort=&state=closed&labels=&milestone=0&assignee=0&poster=0) in accordance to the [decision making process](https://codeberg.org/forgejo-contrib/governance/src/branch/main/DECISION-MAKING.md), as promised when Forgejo was created.

Forgejo Actions, the experimental integrated CI, now runs on the new Forgejo infrastructure which includes two instance. One [for experimenting and debugging](https://next.forgejo.org) and another dedicated to [Forgejo development](https://code.forgejo.org). In a spirit of dogfooding, the [Forgejo runner](https://code.forgejo.org/forgejo/runner) that spawns CI jobs is now [released using Forgejo Actions](https://code.forgejo.org/forgejo/runner/src/branch/main/.forgejo/workflows/release.yml). A new action, [setup-forgejo](https://code.forgejo.org/actions/setup-forgejo) was also created to conveniently spawn a Forgejo instance for the duration of a test.

There also are noteworthy activities in the [wider Forgejo community](https://codeberg.org/forgejo-contrib/delightful-forgejo) not covered in this update.

### Forgejo v1.19 release

On 21 March 2023 [Forgejo v1.19](https://codeberg.org/forgejo/forgejo/releases/tag/v1.19.0-2) was [released](https://forgejo.org/2023-03-release-v1/). It was challenging to figure out how [the new documentation](https://forgejo.org/docs/v1.19/) and additional binaries required for [Forgejo Actions](https://forgejo.org/2023-02-27-forgejo-actions/) fit in.

- The documentation is versioned and maintained in a directory that has the same name as the Forgejo release
- The [Forgejo runner](https://code.forgejo.org/forgejo/runner) is released independently and tested to be compatibility with existing Forgejo releases

Forgejo v1.19 includes all of Gitea v1.19 and the upgrade went smoothly. If Gitea was a Go package with a documented API it would be as easy as upgrading one of the [other Forgejo dependencies](https://codeberg.org/forgejo/forgejo/src/commit/4c132e77ea2cba9a1161f4cfc18b82b9e2e1b35f/go.mod). But it is not and that's where the difficulty of a soft fork resides.

The Forgejo strategy, since its inception, is to rebase [on top of Gitea weekly](https://codeberg.org/forgejo/forgejo/milestones?state=closed&q=rebase). The Forgejo commit series are kept to a minimum (for instance by [squashing related commits](https://codeberg.org/forgejo/forgejo/pulls/590)) and conflicts, if any, are resolved (for instance [changing from less to css](https://codeberg.org/forgejo/forgejo/pulls/552) required reworking the commit implementing the Forgejo themes). It turns out to be sustainable and the associated maintenance work stays the same over time. Considering that hundreds of commits have been merged in Forgejo over the past four months, there were many opportunities for a rebase to go wrong and get stuck because of conflicts. The absence of problems during the Forgejo v1.19 release is a sign that this strategy is working.

When Forgejo started, people stepped up to create the releases. There was no decision making process at the time and no clear way for the community to agree that they could be trusted with this role. They were part of [an interim governance](https://codeberg.org/forgejo/forgejo/src/commit/f46d4279c7b562648bbc17afedfe044b774185ac/CONTRIBUTING/GOVERNANCE.md#interim-forgejo-governance) and pledged to resign as soon as a governance was in place. They fulfilled this promise 2 March 2023 by [applying](https://codeberg.org/forgejo/governance/issues?q=application%20to%20release%20team&type=all&sort=&state=closed&labels=&milestone=0&assignee=0&poster=0) to become legitimate members of the Forgejo release team in accordance to the [decision making process](https://codeberg.org/forgejo-contrib/governance/src/branch/main/DECISION-MAKING.md). They got an agreement a few weeks later and there now is a Forgejo release team agreed upon by the Forgejo community.

### A dedicated Forgejo infrastructure

#### next.forgejo.org

A volatile Forgejo v1.19 instance dedicated to trying new features and demonstrate bugs is now running at https://next.forgejo.org. It is [linked from the website](https://forgejo.org) and in the [bug report template](https://codeberg.org/forgejo/forgejo/issues/new?template=.gitea%2fISSUE_TEMPLATE%2fbug-report.md).

#### code.forgejo.org

The experimental integrated CI that comes with Forgejo v1.19 is not just a new feature, it also depends on:

- a new binary, [Forgejo runner](https://code.forgejo.org/forgejo/runner), that is responsible for running the CI jobs
- a repository of [reusable Free Software Actions](https://code.forgejo.org/actions)

They both need to be tested and it would be convenient if Forgejo Actions was enabled on Codeberg. There would be no need for Codeberg to provide a runner: it can be run and connected independently, using a token specific to a given repository. Forgejo would spawn a runner and it would work without requiring any resource from Codeberg. However, there are at least two reasons for waiting until Forgejo v1.20 or v1.21 before enabling this feature on Codeberg, even without providing runners to Codeberg users:

- it is still experimental and fragile
- it may have security issues

To solve that problem an instance of Forgejo was installed at https://code.forgejo.org. It has a registration open to Codeberg users and is dedicated to Forgejo related development. It has Actions enabled and provides a runner to selected repositories.

#### The devops team

These machines need devops and [applications to the devops team](https://codeberg.org/forgejo/governance/issues?type=all&state=open&labels=&milestone=0&assignee=0&poster=0&q=devops+team) are in progress. Their role is, in a nutshell, to keep all machines Forgejo depends on in a healthy state. That includes the [new machine](https://codeberg.org/forgejo/sustainability#hardware) made available to the Forgejo project, which could be used to host code.forgejo.org.

### Dogfooding Forgejo Actions

Forgejo provides some essential Actions in a [dedicated organization](https://code.forgejo.org/actions). As one can expect of any function in Go or Python, Actions are not immune to typos, regressions or subtle misbehavior, just like the software they are designed to test. Testing is even more important in a distributed environment where each Action is maintained by different groups of people and have their own lifecycle.

The CI of the [Forgejo Runner](https://code.forgejo.org/forgejo/runner/) itself relies on workflows that use actions for:

- Running [unit tests](https://code.forgejo.org/forgejo/runner/src/branch/main/.forgejo/workflows/test.yml)
- [Publishing](https://code.forgejo.org/forgejo/runner/src/branch/main/.forgejo/workflows/release.yml) its own [releases](https://code.forgejo.org/forgejo/runner/releases)
- [Integration testing](https://code.forgejo.org/forgejo/runner/src/branch/main/.forgejo/workflows/release.yml) to verify it performs as intended with a live Forgejo instance

The Forgejo Runner integration tests [uses](https://codeberg.org/forgejo/runner/src/commit/bcd6096e5b0fb701eca85a391c0dafb0f606fc85/.forgejo/workflows/integration.yml#L16-L21) a new [setup-forgejo action](https://code.forgejo.org/actions/setup-forgejo). It runs a Forgejo instance for the duration of the test, very much like a MySQL or PostgreSQL service. It can conveniently be used by software that depends on Forgejo, for instance to test interactions with the Forgejo API instead of relying on mocks.

The `setup-forgejo` action uses itself, recursively, for [integration testing](https://code.forgejo.org/actions/setup-forgejo/src/branch/main/.forgejo/workflows/integration.yml) with two levels of nesting:

- Forgejo 0: https://code.forgejo.org is a Forgejo instance that hosts [setup-forgejo](https://code.forgejo.org/actions/setup-forgejo).
- Forgejo 1: when a pull request is proposed `setup-forgejo`, the CI job creates a brand new Forgejo instance with a runner to experiment with the proposed change.
- Forgejo 2: The [experiment](https://code.forgejo.org/actions/setup-forgejo/src/branch/main/testdata/sanity-checks/.forgejo/workflows/test.yml) consists of running `setup-forgejo` and verify the Forgejo instance it creates actually works as expected.

This is [made possible](https://code.forgejo.org/forgejo/act/commit/bc24ebba1bceacf5c2d890caef9ad824ad9d2f80) by enabling [LXC](https://linuxcontainers.org/) system containers in the runner. With the additional benefit of allowing `systemd` to run, installing `docker` instead of `dind` etc.

### The Forgejo Community is healing

In February 2023 someone new (who wasn't a contributor that the project is relying on) joined the chat and issue tracker, spoke repeatedly in ways that was hurtful/painful to Forgejo community members, and did not seem to have capacity to speak more sensitively, despite offers for support and repeated requests. It was eventually decided to [remove this person from the project](https://codeberg.org/forgejo/governance/issues/8) during a year. But the tension they created did not dissipate instantly. In March 2023 it distracted community members from productive and important work on governance, strategy and development. Some community members went silent, others were on edge and [more moderation actions were taken](https://codeberg.org/forgejo/governance/issues/16).

It is in the nature of inclusive communities to be subject to that sort of trouble in their infancy, when they are still fragile. Some can be destroyed for good, others may choose to be less inclusive to better protect the members of their inner circles. Something different is happening in Forgejo. Late March it went back to be the quiet and productive space it once was. Community members who were silent during the troubles came back. A [moderation process](https://codeberg.org/forgejo/governance/pulls/17/files) was proposed and was field tested. It is a little early to be sure but it appears the community is healing and is now better protected from negative influence, without sacrificing on inclusiveness.

### Grant applications

As a followup to questions sent by NLnet on [a grant application](https://codeberg.org/forgejo/sustainability/issues/1) to create a Forgejo distribution, potential beneficiaries worked on a [detailed project plan](https://codeberg.org/forgejo/sustainability/issues/1#issuecomment-855819). It was sent late March and it will take a few weeks before a reply is received to determine if it is allowed to proceed to the next step.

Another [grant application focused on improving Forgejo UI/UX](https://codeberg.org/forgejo/sustainability/issues/4#issuecomment-840994) was declined.

### We Forge

Forgejo is a **community of people** who contribute in an inclusive environment. We forge on an equal footing, by reporting a bug, voicing an idea in the chatroom or implementing a new feature. The following list of contributors is meant to reflect this diversity and acknowledge all contributions since the last monthly report was published. If you are missing, please [ask for an update](https://codeberg.org/forgejo/website/issues/new).

- https://codeberg.org/aral
- https://codeberg.org/caesar
- https://codeberg.org/circlebuilder
- https://codeberg.org/crystal
- https://codeberg.org/cweiske
- https://codeberg.org/dachary
- https://codeberg.org/delvh
- https://codeberg.org/earl-warren
- https://codeberg.org/Eragon
- https://codeberg.org/fnetX
- https://codeberg.org/fr33domlover
- https://codeberg.org/fsologureng
- https://codeberg.org/GamePlayer-8
- https://codeberg.org/Gusted
- https://codeberg.org/jan_x7
- https://codeberg.org/jerger
- https://codeberg.org/KaKi87
- https://codeberg.org/KOLANICH
- https://codeberg.org/n0toose
- https://codeberg.org/NextFire
- https://codeberg.org/oliverpool
- https://codeberg.org/redwerkz
- https://codeberg.org/Ryuno-Ki
- https://codeberg.org/SHuRiKeN
- https://codeberg.org/tallship
- https://codeberg.org/TheEvilSkeleton
- https://codeberg.org/trymeout
- https://codeberg.org/uda
- https://codeberg.org/viceice
- https://codeberg.org/wxiaoguang
- https://codeberg.org/xtex
- https://codeberg.org/xy
- https://codeberg.org/zander

A **minority of Forgejo contributors earn a living** by implementing the roadmap co-created by the Forgejo community, see [the sustainability repository](https://codeberg.org/forgejo/sustainability) for the details.
