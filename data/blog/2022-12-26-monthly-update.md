---
title: First forgejo monthly update - December 2022
publishDate: 2022-12-26
tags: ['news']
excerpt: Forgejo was announced 15 December 2022, here is how it happened.
---

Forgejo was [announced in December 2022](https://forgejo.org/2022-12-15-hello-forgejo/) and this report explains how it happened. It is the creation of a collective of people from diverse backgrounds united towards a common goal: enabling Free Software contributors with Free Software tools. The software forge it provides can be downloaded and used right away and it has a unique non-technical feature: the community that drives its roadmap can be trusted to put the interest of the general public first. Forging Free Software is not just about writing code, it is a [social construct](https://coding.social/) that involves everyone in a continuous loop.

This first monthly report is meant to provide a high-level update of what happened in Forgejo, for people who want to follow its progress from a distance. Fact checking is made easy with links to the issue trackers or chat rooms where the action happened: everything in Forgejo since its inception has been 100% transparent. Video conferences are also [organized monthly](https://codeberg.org/forgejo/meta/issues/36) for Forgejo community members to ask questions.

### Bootstrap

After [Gitea Ltd confirmed the takeover of the Gitea project](https://gitea-open-letter.coding.social/#gitea-ltd-confirms-its-takeover-of-the-gitea-project) on 30 October 2022, a group of people proposed that [Codeberg e.V.](https://docs.codeberg.org/getting-started/what-is-codeberg/#what-is-codeberg-e.v.%3F) should become the custodian of a fork of Gitea. The proposal was [accepted 16 November 2022](https://codeberg.org/forgejo/meta/issues/3#issuecomment-688648):

- Codeberg e.V. is in control of the domains and the trademarks (if any)
- Forgejo contributors are a self-governed group of people (a "gremium" in the Codeberg e.V. parlance)
- The Codeberg e.V. general assembly reviews if Forgejo is aligned with its mission on a yearly basis, based on the monthly reports Forgejo publishes

The essential conditions for a fork to succeed were met and Forgejo was able to be bootstrapped in a sustainable way:

- A well-known and trusted organization agreed to support and use Forgejo
- A group of people with the right skills and stamina committed to provide a long-term effort
- The strategy to create a soft fork was a good match for the Forgejo contributors workforce

It took another month for the bootstrap to complete and Forgejo was [announced 15 December 2022](https://forgejo.org/2022-12-15-hello-forgejo/).

### Development and Distribution

From a technical point of view, Forgejo is a [drop-in replacement for Gitea](https://forgejo.org/download/): it can be used without any modification by simply replacing the Gitea binary (or container image).

The first technical work that was done was to replace the Drone (which is not Free Software) release pipeline used by Gitea with a pipeline based on [Woodpecker CI](https://woodpecker-ci.org/). It produced a release candidate [for Forgejo 1.18.0-rc1-1](https://forgejo.org/releases/) and is ready for the 1.18 release that will include:

- binaries for GNU/Linux amd64, arm64 and armv6 (which can also be used on armv7)
- container images for amd64 and arm64, either running as root or rootless

It will include some [Forgejo branding changes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/CONTRIBUTING/WORKFLOW.md#branding) and default settings to [enhance privacy](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/CONTRIBUTING/WORKFLOW.md#privacy). And more importantly, it provides a non-technical feature: trust in a sustainable non-profit community to further the interest of the general public.

The forge federation features is a focus of Forgejo. Although it is not ready to be released with version 1.18, active development started to [support ActivityPub and F3](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/CONTRIBUTING/WORKFLOW.md#federation-https-codeberg-org-forgejo-forgejo-issues-labels-79349).

### User Centric Roadmap

Already within a few days [dozens of ideas for improving Forgejo](https://codeberg.org/forgejo/forgejo/issues?labels=78139) were submitted. They express the needs and preferences of Free Software contributors with regards to how they forge their software. This raw material is input to the roadmap and will lead Forgejo on a path that is unique and different than what corporate forges like Github and Gitlab have to offer. Forgejo will place the community firmly in control of project direction.

How can Forgejo go from an unsorted pile of ideas to an exciting roadmap? What innovations will inspire contributors? And how do these ideas transform into concrete features that will be implemented, instead of lingering as open issues on the backlog? To address people's needs and be most beneficial for the commons and public interest, Forgejo will follow a structured method of [User Research](https://jdittrich.github.io/userNeedResearchBook/), align with [Social Coding Movement](https://coding.social/), and contribute back insights about Free Software development to evolve the movement's best-practices.

A [repository was created](https://codeberg.org/forgejo/user-research) to bootstrap that effort. It requires a kind of skill that is rare but Forgejo is in a lucky position and [benefits from mentoring](https://codeberg.org/forgejo/meta/issues/20) and [past examples related to forge federation](https://lab.forgefriends.org/fedeproxy/ux/-/wikis/2021-06-user-research-report). Some Forgejo contributors also intend to participate in the [OSD devroom](https://discourse.opensourcedesign.net/t/fosdem-2023/3093) early 2023 during [FOSDEM](https://fosdem.org/2023/).

The [Forgejo accessibility team was created](https://codeberg.org/forgejo/meta/src/branch/readme/TEAMS.md#accessibility) and just [began User Research](https://codeberg.org/forgejo/user-research/src/branch/master/accessibility) to figure out what matters most to people who struggle with Forgejo because of its accessibility shortcomings.

### Governance

The Forgejo [domains](https://codeberg.org/forgejo/meta/issues/41) are owned by [Codeberg e.V.](https://codeberg.org/Codeberg/org/src/branch/main/en/bylaws.md). Forgejo is therefore ultimately under the control of Codeberg e.V. and its governance. However, although Codeberg e.V. is committed to use and host Forgejo, it is expected that Forgejo defines its own governance.

The process to [define the governance](https://codeberg.org/forgejo/meta/issues/19) is in progress and expected to take weeks if not months. The first two meetings happened ([24 November](https://codeberg.org/forgejo/meta/issues/19#issuecomment-694460) and [10 December](https://codeberg.org/forgejo/meta/issues/19#issuecomment-711201)). Discussions followed to [define how decisions are proposed and enacted](https://codeberg.org/forgejo/meta/issues/19#issuecomment-722095).

While the governance is being defined, there was a need to establish an interim Forgejo governance for safeguarding credentials, enforcing the Code of Conduct and ensuring security vulnerabilities are handled responsibly for the Forgejo releases. All people with a role in the interim Forgejo governance pledge to resign as soon as the Forgejo governance is in place. The people and teams that are part of the interim governance are [listed publicly](https://codeberg.org/forgejo/meta/src/branch/readme/TEAMS.md).

### Branding

The support for theming in Gitea is brittle and rebranding is a challenge.

The easy part was the choice of the Forgejo name, which started [6 November](https://codeberg.org/forgejo/meta/issues/1) and took ten days, with multiple rounds of discussion. The forgejo.org domain was [then acquired](https://codeberg.org/forgejo/meta/issues/35) and [Codeberg e.V. has control over the registrar account](https://codeberg.org/forgejo/meta/issues/41#issuecomment-726673). A logo was then [created](https://codeberg.org/forgejo/meta/issues/23#issuecomment-693408) as well as [a mascot](https://codeberg.org/forgejo/meta/issues/56).

The more substantial work then started in [a dedicated branch](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/CONTRIBUTING/WORKFLOW.md#branding). The rebranding touches many areas and needs to be done in such a way that it is transparent for people upgrading from Gitea. It involves filenames, environment variables, configuration settings, web templates, ... Not only is it tedious, it is also subject to conflicts when rebasing on top of Gitea.

### Localization

The Gitea codebase is internationalized in a way that follows the expected standards. But the localization (i.e. the translations in various languages) relies on a proprietary service that has the translation community locked in. Since Forgejo is committed to exclusively use Free Software dependencies and services, it has to use an alternative.

Fortunately, [Weblate](https://weblate.org/) is a quality self-hostable translation platform and an instance [is provided by Codeberg](https://translate.codeberg.org/). Initial work started in a [dedicated branch](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/CONTRIBUTING/WORKFLOW.md#internationalization-https-codeberg-org-forgejo-forgejo-issues-labels-82637), for the technical part.

The most difficult part will be to bootstrap a community of translator working on Weblate instead of the proprietary platform that Gitea depends on. Ideas on how to achieve that in an incremental way are [most welcome](https://codeberg.org/forgejo/meta/issues/72).

### Communication and Code of Conduct

As a new project Forgejo had to create new communication channels:

- a static website at https://forgejo.org built by a CI pipeline from [a repository](https://codeberg.org/Forgejo/website) for collaborative editing.
- a social account at https://floss.social/@forgejo where content is either published after a consensus is reached from the Forgejo community or signed by the author trusted with access to the account.
- a [Matrix space](https://matrix.to/#/#forgejo:matrix.org) where the main room welcomes Forgejo users to answer any question they may have and development rooms where technical discussions happen between people actively working.
- all other communications happen on the forge itself, in a spirit of dogfooding. For instance, although it may be more convenient to use a forum such as Discourse for informal discussions or a task manager to organize the work, the issue tracker is used instead.

All communication happens in public, transparently, so a newcomer can immediately get up to speed by reading the backlog. A [Code of Conduct](https://codeberg.org/forgejo/code-of-conduct) was adopted [very early on](https://codeberg.org/forgejo/meta/issues/13) and applies to all spaces under the responsibility of the Forgejo community. It is essential to create an inclusive environment where everyone can feel safe and a [Well Being](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/CONTRIBUTING/COC.md) team is here to help if needed.

A particular effort was made when drafting the [Forgejo announcement](https://forgejo.org/2022-12-15-hello-forgejo/) and the [Codeberg blog post](https://blog.codeberg.org/codeberg-launches-forgejo.html) to explain that Forgejo is meant to reunite the community. It does not compete with Gitea, it builds upon it.

### Sustainability

The ideal Free Software project is sustainable because it involves many independent individuals who have the means to work for the interest of the general public, in the long run. It is unhealthy when it relies heavily on overworked volunteers who eventually burn out. It is also unhealthy when it is exclusively under the control of a single for-profit organization that works for the benefit of its shareholders.

Forgejo is set to create a healthy balance that involves:

- A majority of independent volunteers who are in control of Forgejo via a governance that empowers them. They are representatives of the general public and ultimately the only ones who legitimate to make decisions.
- A minority of people who earn a living by implementing a roadmap co-created by the Forgejo community.

As of today there are just a handful of [people paid for their work](https://codeberg.org/forgejo/sustainability) and an order of magnitude more who volunteer. A grant application was [submitted December 1st](https://codeberg.org/forgejo/sustainability/issues/1) while [another is ongoing](https://forum.forgefriends.org/t/nlnet-grant-application-for-federation-in-gitea-deadline-august-1st-2022/823) and already funds some of the work towards forge federation.

It is unclear if Forgejo will manage to be sustainable while so many other Free Software projects struggle. But maybe it will because it does something unique: being transparent about its funding. Not translucent: transparent.
