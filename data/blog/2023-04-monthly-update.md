---
title: Forgejo monthly update - April 2023
publishDate: 2023-05-20
tags: ['news']
excerpt: Codeberg upgraded to Forgejo v1.19 and, as can be expected from the largest instance in existence, hit a performance issue which was dealt with quickly. The security of Forgejo actions and the release process was a focus with solutions being found that involve LXC and a VPN. Federation is taking a step forward with the F3 driver entering the Forgejo development branch.
---

Codeberg upgraded to Forgejo v1.19 and, as can be expected from the largest instance in existence, [hit a performance issue](https://codeberg.org/forgejo/forgejo/issues/680) which was dealt with quickly.

The security of Forgejo actions and the release process was a strong focus. It is critical for Forgejo actions to be used in production safely: a random Codeberg user must be able to rely on the CI by submitting a pull request without putting the underlying infrastructure at risk. A solution based on [LXC](https://linuxcontainers.org/lxc/) is implemented. To improve the security of the release pipeline, the devops team is deploying a Forgejo instance and runner on a dedicated hardware behind a VPN to sign the binaries.

There also are noteworthy activities in the [wider Forgejo community](https://codeberg.org/forgejo-contrib/delightful-forgejo) not covered in this update.

### Forgejo v1.19 point releases

Three point releases were published ([1.19.1-0](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#1-19-1-0), [1.19.2-0](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#1-19-2-0) and [1.19.3-0](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#1-19-3-0)).

Codeberg.org waited until Forgejo v1.19 proved to be stable enough to upgrade. It did not go smoothly because of a [performance issue](https://codeberg.org/forgejo/forgejo/issues/680). However, a diagnostic and a workaround were found and applied within 24h. This problem impacts all Forgejo v1.19 instances but was only noticed in Codeberg.org because of its scale. It will be properly fixed in the next point release.

To make them easier to digest for the Forgejo administrators and users, the release notes now have detailed explanations of the most prominent changes. This is specially useful for security fixes to better understand their impact and evaluate the urgency of the update on a case by case basis.

The [Forgejo semantic version](https://forgejo.org/docs/v1.19/user/semver/) is included in the release notes as a reminder that it can be used for scripting to automatically identify a release that includes a breaking change and avoid unexpected downtime.

### A step towards federation

For federation to happen, Forgejo instances must be able to mirror each other. It is different from being able to migrate a project from one instance to another. The [F3](https://forum.forgefriends.org/t/about-the-friendly-forge-format-f3/681) reference [implementation](https://lab.forgefriends.org/friendlyforgeformat/gof3), which is still in development stage, will allow for mirroring a single issue as well as an entire project.

A [F3 driver](https://codeberg.org/forgejo/forgejo/commits/branch/forgejo-f3) was developed for Forgejo and merged into the development branch. It is not ready for experimenting but it passes some integration tests and will be part of the weekly round of rebase together with the other Forgejo feature branches.

### Forgejo actions progress

As an experimental feature, [Forgejo actions](https://forgejo.org/2023-02-27-forgejo-actions/) keeps improving. The [Forgejo runner](https://codeberg.org/forgejo/runner) saw a few more releases, [using itself](https://codeberg.org/forgejo/runner/src/branch/main/.forgejo/workflows/release.yml) in a nicely recursive way.

It is developed on [code.forgejo.org](https://code.forgejo.org/) which runs Forgejo v1.19 and includes a CLI that allows it to obtain a registration token: `forgejo actions generate-runner-token`. It was not documented or advertised which led to confusion when it was changed to match the Gitea implementation that came later. It is fine when a feature is experimental. But it was a reminder that care must be taken when introducing new features in Forgejo so they do not conflict with Gitea and other dependencies.

[LXC](https://linuxcontainers.org/lxc/) is confirmed to be a good option for job isolation. The runner is based on [ACT](https://github.com/nektos/act/) which is designed to run jobs on a local machine and assumes its user is trusted. When running a job from a pull request authored by less trusted users, their ability to use docker as a mean to access files from the host becomes problematic. One option is to spawn a [dind](https://hub.docker.com/_/docker/tags?page=1&name=dind) on every job. Another is to run the job in LXC.

### Hardware infrastructure

The [devops team](https://codeberg.org/forgejo/governance/src/branch/main/TEAMS.md#devops) was [appointed](https://codeberg.org/forgejo/governance/pulls/21) in accordance to the [decision making process](https://codeberg.org/forgejo-contrib/governance/src/branch/main/DECISION-MAKING.md). It is responsible for the day to day operations of https://code.forgejo.org, https://next.forgejo.org and https://forgejo-ci.codeberg.org.

A new hardware at `octopuce.forgejo.org` is being deployed and provides a VPN for the release team to secure the last step which involves a pipeline to sign the binaries. It reduces the [attack surface](https://en.wikipedia.org/wiki/Attack_surface) compared to using a public facing Forgejo instance to do the same. It will first be used for releasing the Forgejo runner and, when stabilized, Forgejo itself.

## Grant applications

The NLnet grant application focused on producing quality Forgejo releases that are essential for federation to happen was [accepted to the next step](https://codeberg.org/forgejo/sustainability/issues/1#issuecomment-911709). This is the last one and it will few weeks before the final decision is known.

## Moderation team

The [moderation team](https://codeberg.org/forgejo/governance/src/branch/main/TEAMS.md#moderation) now has [one member](https://codeberg.org/forgejo/governance/pulls/23). All moderation actions are [bound to the moderation process](https://codeberg.org/forgejo/governance/src/branch/main/MODERATION-PROCESS.md) for transparency and audit by the Forgejo community.

## Licensing

It was decided on principle that [Forgejo would accept copylefted contributions](https://codeberg.org/forgejo/governance/pulls/20). Before it happens a decision will need be made on the choice of a license. Until then the licensing terms of Forgejo are not modified.

## We Forge

Forgejo is a **community of people** who contribute in an inclusive environment. We forge on an equal footing, by reporting a bug, voicing an idea in the chatroom or implementing a new feature. The following list of contributors is meant to reflect this diversity and acknowledge all contributions since the last monthly report was published. If you are missing, please [ask for an update](https://codeberg.org/forgejo/website/issues/new).

- https://codeberg.org/bat
- https://codeberg.org/borega
- https://codeberg.org/braydofficial
- https://codeberg.org/caesar
- https://codeberg.org/carlokok
- https://codeberg.org/circlebuilder
- https://codeberg.org/crystal
- https://codeberg.org/dachary
- https://codeberg.org/DanielGibson
- https://codeberg.org/earl-warren
- https://codeberg.org/fluzz
- https://codeberg.org/fnetX
- https://codeberg.org/fr33domlover
- https://codeberg.org/Gusted
- https://codeberg.org/KaKi87
- https://codeberg.org/kuhnchris
- https://codeberg.org/LordMZTE
- https://codeberg.org/Lvceo
- https://codeberg.org/msrd0
- https://codeberg.org/n0toose
- https://codeberg.org/NextFire
- https://codeberg.org/oliverpool
- https://codeberg.org/silverwind
- https://codeberg.org/soas
- https://codeberg.org/spooky-overwrite
- https://codeberg.org/Valenoern
- https://codeberg.org/viceice
- https://codeberg.org/WRMSR
- https://codeberg.org/wxiaoguang
- https://codeberg.org/xtex
- https://codeberg.org/xy
- https://codeberg.org/zander

A **minority of Forgejo contributors earn a living** by implementing the roadmap co-created by the Forgejo community, see [the sustainability repository](https://codeberg.org/forgejo/sustainability) for the details.
