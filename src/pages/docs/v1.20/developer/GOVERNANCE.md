---
layout: '~/layouts/Markdown.astro'
title: Governance
license: 'CC-BY-SA-4.0'
---

## Codeberg e.V. custodian of the domains

The Forgejo [domains](https://codeberg.org/forgejo/meta/issues/41) are owned by the democratic non-profit dedicated to Free Software [Codeberg e.V.](https://codeberg.org/Codeberg/org/src/branch/main/en/bylaws.md). Forgejo is therefore ultimately under the control of Codeberg e.V. and its governance. However, although Codeberg e.V. is committed to use and host Forgejo, it is expected that Forgejo defines its own governance, in a way that is compatible with the Codeberg e.V. governance.

## Forgejo Governance

Forgejo was bootstraped in November 2022 and [defined its governance](https://codeberg.org/forgejo/governance/src/branch/main/README.md) in the following months.
