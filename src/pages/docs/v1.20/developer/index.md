---
layout: '~/layouts/Markdown.astro'
title: 'Forgejo developer guide'
---

This area will be targeted to people who want to hack Forgejo and adjust it to
their needs.

- For everyone involved
  - [Code of Conduct](COC)
  - [Bugs, features, security and others discussions](DISCUSSIONS)
  - [Governance](GOVERNANCE)
  - [Sustainability and funding](https://codeberg.org/forgejo/sustainability/src/branch/master/README)
- For contributors
  - [Developer Certificate of Origin (DCO)](DCO)
  - [Development workflow](WORKFLOW)
  - [code.forgejo.org](code-forgejo-org)
  - [Forgejo runner implementation notes](forgejo-runner)
- For maintainers
  - [Hardware infrastructure](infrastructure)
  - [Release management](RELEASE)
  - [Secrets](SECRETS)
