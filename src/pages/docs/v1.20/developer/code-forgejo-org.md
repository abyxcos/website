---
layout: '~/layouts/Markdown.astro'
title: code.forgejo.org
license: 'CC-BY-SA-4.0'
---

https://code.forgejo.org is a Forgejo instance running the latest stable
version. It is dedicated to hosting the following repositories:

- Default Forgejo Runner actions https://code.forgejo.org/actions
- Forgejo Runner https://code.forgejo.org/forgejo/runner
- [ACT](https://github.com/nektos/act) soft fork https://code.forgejo.org/forgejo/act
- [Infrastructure as code](https://enough-community.readthedocs.io) used to deploy code.forgejo.org https://code.forgejo.org/forgejo/infrastructure
- [Infrastructure as code](https://enough-community.readthedocs.io) secrets in a private repository

To make these repositories easier to find, the following push mirrors are in place:

- https://code.forgejo.org/forgejo/runner => https://codeberg.org/forgejo/runner
- https://code.forgejo.org/forgejo/act => https://codeberg.org/forgejo/act
