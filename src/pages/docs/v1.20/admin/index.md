---
layout: '~/layouts/Markdown.astro'
title: 'Forgejo administrator guide'
---

These documents are targeted to people who run Forgejo on their machines.

- [Seek Assistance](seek-assistance)
- [Installation](installation)
- [Database Preparation](database-preparation)
- [Configuration Cheat Sheet](config-cheat-sheet)
- [Upgrade guide](upgrade)
- [Command Line](command-line)
- [Reverse Proxy](reverse-proxy)
- [Email setup](email-setup)
- [Incoming Email](incoming-email)
- [Logging Configuration](logging-documentation)
- [Actions](actions)
